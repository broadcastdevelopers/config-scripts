# Config Scripts

## ReactJS

Voer `exec 3<&1;bash <&3 <(curl https://bitbucket.org/broadcastdevelopers/config-scripts/raw/master/react.sh 2> /dev/null)` uit in de folder van je ReactJS project

## NodeJS

Voer `exec 3<&1;bash <&3 <(curl https://bitbucket.org/broadcastdevelopers/config-scripts/raw/master/nodejs.sh 2> /dev/null)` uit in de folder van je NodeJS project

## Visual Studio Code settings

```json
{
  "workbench.startupEditor": "newUntitledFile",
  "workbench.colorTheme": "Material Theme Darker High Contrast",
  "editor.tabSize": 2,
  "editor.formatOnSave": true,
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",
  "javascript.format.enable": false,
  "prettier.eslintIntegration": true
}
```
